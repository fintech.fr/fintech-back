# fintech-back

#Installation

Make sure your apache and your mysql are stopped.
Install docker & docker-compose then run:
```shell script
sudo docker-compose up -d
```

Copy the htdocs/.env.local file to htdocs/.env

Run: 
```shell script
sudo docker-compose exec php bash  // (connect you to the docker environnement)

php bin/console doctrine:database:create
php bin/console doctrine:schema:create
php bin/console doctrine:migrations:migrate
```

Add "127.0.0.1 fintech.docker" to your /etc/hosts

Generate JWT keys:

```shell script
docker-compose exec php sh -c '
    set -e
    apt install openssl
    mkdir -p config/jwt
    jwt_passhrase=$(grep ''^JWT_PASSPHRASE='' .env | cut -f 2 -d ''='')
    echo "$jwt_passhrase" | openssl genpkey -out config/jwt/private.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
    echo "$jwt_passhrase" | openssl pkey -in config/jwt/private.pem -passin stdin -out config/jwt/public.pem -pubout
    chmod 755 -R config/jwt/
'
```

Front/API: fintech.docker
Phpmyadmin: fintech.docker:8001
Maildev: fintech.docker:8002