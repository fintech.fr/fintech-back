<?php
namespace App\Service;

use App\Entity\User;
use App\Exception\EmailPatternException;
use App\Exception\EmailTakenException;
use App\Exception\MissingParameterException;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UserCreationHandler
 * @package App\Service
 */
class UserCreationHandler
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * UserCreationHandler constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * User role string
     */
    private const ROLE_USER = 'ROLE_USER';

    /**
     * Handle user data before persisting to database
     *
     * @param User $data
     * @throws EmailTakenException
     * @throws EmailPatternException
     * @throws MissingParameterException
     */
    public function handle(User $data) {
        $this->verifyNoMissingParams($data);
        $this->verifyEmailUniqueness($data->getEmail());
        $this->verifyEmailPattern($data->getEmail());
        $this->encryptPassword($data);
        $this->verifyRoleData($data);
    }

    /**
     * Verify missing parameters
     *
     * @param User $data
     * @throws MissingParameterException
     */
    public function verifyNoMissingParams(User $data){
        if(
            $data->getUsername() == null ||
            $data->getEmail() == null ||
            $data->getPassword() == null ||
            empty($data->getRoles())
        ) {
            throw new MissingParameterException();
        }
    }

    /**
     * Verify email uniqueness
     * @param String $email
     * @throws EmailTakenException
     */
    public function verifyEmailUniqueness(String $email){
        $userFound = $this->entityManager->getRepository(User::class)->findOneBy(
            [ 'email' => $email]
        );
        if($userFound){
            throw new EmailTakenException();
        }
    }

    /**
     * Verify email pattern
     * @param String $email
     * @throws EmailPatternException
     */
    public function verifyEmailPattern(String $email){
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            throw new EmailPatternException();
        }
    }

    /**
     * Encrypt user password
     * @param User $User
     */
    public function encryptPassword(User $User){
        $User->setPassword(password_hash($User->getPassword(),PASSWORD_BCRYPT));
    }

    /**
     * Keep only ROLE_USER / remove forbidden roles
     *
     * @param User $data
     */
    public function verifyRoleData(User $data) {
        $tempRoles = $data->getRoles();
        $roleUserCount = 0;
        foreach ($tempRoles as $index => $role){
            if($role !== self::ROLE_USER || $roleUserCount != 0){
                unset($tempRoles[$index]);
            }
            else{
                $roleUserCount ++;
            }
        }
        $data->setRoles($tempRoles);
    }
}
