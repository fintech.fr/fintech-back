<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Controller\UserCreation;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class User
 * @package App\Entity
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 *     collectionOperations={
 *      "post"={
 *          "method"="POST",
 *          "path"="/users",
 *          "controller"=UserCreation::class,
 *      }
 *     },
 *     itemOperations={
 *       "get",
 *      }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("read")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"read", "write"})
     * @var String
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     * @Groups("write")
     * @var String (Hashed password)
     */
    private $password;

    /**
     * @ORM\Column(type="array")
     * @Groups("read")
     * @var array
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string")
     * @Groups({"read", "write"})
     * @var String
     */
    private $firstName;

    /**
     * @ORM\Column(type="string")
     * @Groups({"read", "write"})
     * @var String
     */
    private $lastName;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read", "write"})
     * @var DateTime
     */
    private $birthday;

    /**
     * @ApiSubresource
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="author")
     * @Groups("read")
     */
    private $projects;

    /**
     * @ApiSubresource()
     * @ORM\OneToMany(targetEntity="App\Entity\Investment", mappedBy="author")
     */
    private $investments;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
        $this->investments = new ArrayCollection();
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';
        return array_unique($roles);
    }

    /**
     * @inheritDoc
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // not needed with bcrypt
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->getEmail();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return String
     */
    public function getEmail(): String
    {
        return $this->email;
    }

    /**
     * @param String $email
     */
    public function setEmail(String $email): void
    {
        $this->email = $email;
    }

    /**
     * @param String $password
     */
    public function setPassword(String $password): void
    {
        $this->password = $password;
    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    /**
     * @return String
     */
    public function getFirstName(): String
    {
        return $this->firstName;
    }

    /**
     * @param String $firstName
     */
    public function setFirstName(String $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return String
     */
    public function getLastName(): String
    {
        return $this->lastName;
    }

    /**
     * @param String $lastName
     */
    public function setLastName(String $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return DateTime
     */
    public function getBirthday(): DateTime
    {
        return $this->birthday;
    }

    /**
     * @param DateTime $birthday
     */
    public function setBirthday(DateTime $birthday): void
    {
        $this->birthday = $birthday;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setAuthor($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            // set the owning side to null (unless already changed)
            if ($project->getAuthor() === $this) {
                $project->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Investment[]
     */
    public function getInvestments(): Collection
    {
        return $this->investments;
    }

    public function addInvestment(Investment $investment): self
    {
        if (!$this->investments->contains($investment)) {
            $this->investments[] = $investment;
            $investment->setAuthor($this);
        }

        return $this;
    }

    public function removeInvestment(Investment $investment): self
    {
        if ($this->investments->contains($investment)) {
            $this->investments->removeElement($investment);
            // set the owning side to null (unless already changed)
            if ($investment->getAuthor() === $this) {
                $investment->setAuthor(null);
            }
        }

        return $this;
    }
}