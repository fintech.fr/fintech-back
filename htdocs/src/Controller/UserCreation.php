<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\UserCreationHandler;

/**
 * Class ApiUserCreation
 * @package App\Controller
 */
class UserCreation
{
    private $apiUserCreationHandler;

    /**
     * UserCreation constructor.
     * @param UserCreationHandler $apiUserCreationHandler
     */
    public function __construct(UserCreationHandler $apiUserCreationHandler)
    {
        $this->apiUserCreationHandler = $apiUserCreationHandler;
    }

    /**
     * Invoke function
     *
     * @param User $data
     * @return User
     * @throws \App\Exception\EmailPatternException
     * @throws \App\Exception\EmailTakenException
     * @throws \App\Exception\MissingParameterException
     */
    public function __invoke(User $data)
    {
        $this->apiUserCreationHandler->handle($data);
        return $data;
    }
}
