<?php

namespace App\Exception;

/**
 * Class EmailPatternException
 * @package App\Exception
 */
class EmailPatternException extends \Exception
{
    /**
     * EmailPatternException constructor.
     */
    public function __construct()
    {
        parent::__construct("The given email is not valid.", 0, null);
    }
}