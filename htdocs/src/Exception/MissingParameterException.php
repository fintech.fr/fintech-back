<?php

namespace App\Exception;

/**
 * Class MissingParameterException
 * @package App\Exception
 */
class MissingParameterException extends \Exception
{
    /**
     * MissingParameterException constructor.
     */
    public function __construct()
    {
        parent::__construct("One or multiple parameters are missing.", 0, null);
    }
}