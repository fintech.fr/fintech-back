<?php

namespace App\Exception;

/**
 * Class EmailTakenException
 * @package App\Exception
 */
class EmailTakenException extends \Exception
{
    /**
     * EmailTakenException constructor.
     */
    public function __construct()
    {
        parent::__construct("This email is already taken by another user", 0, null);
    }
}