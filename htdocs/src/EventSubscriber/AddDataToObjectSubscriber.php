<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Investment;
use App\Entity\Project;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class AddDataToObjectSubscriber
 * @package App\Listeners
 */
class AddDataToObjectSubscriber implements EventSubscriberInterface
{
    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtManager;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager, UserRepository $userRepository)
    {
        $this->jwtManager = $jwtManager;
        $this->tokenStorage = $tokenStorage;
        $this->userRepository = $userRepository;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['completeData', EventPriorities::PRE_WRITE]
        ];
    }

    /**
     * Complete object data before persist to database
     *
     * @param ViewEvent $event
     */
    public function completeData(ViewEvent $event)
    {
        $object = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if($method !== Request::METHOD_POST) {
            return;
        }

        if($object instanceof Project || $object instanceof Investment) {
            try{
                $jwt = $this->jwtManager->decode($this->tokenStorage->getToken());
                if(isset($jwt['id'])) {
                    $user = $this->userRepository->find($jwt['id']);
                    $object->setAuthor($user);
                    $object->setCreationDate(new \DateTime('now'));
                } else {
                    throw new BadRequestHttpException("Project owner cannot be found");
                }
            } catch (\Exception $e){
                throw new BadRequestHttpException("Project owner cannot be found");
            }
        }
    }
}